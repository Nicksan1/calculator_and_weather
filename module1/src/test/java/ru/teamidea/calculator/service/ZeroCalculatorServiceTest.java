package ru.teamidea.calculator.service;

import org.junit.jupiter.api.Test;
import ru.teamidea.calculator.exception.NumberLessThanZeroException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ZeroCalculatorServiceTest {

    private final ZeroCalculatorService counter = new ZeroCalculatorService();

    @Test
    public void countLastZerosOfFactorialOfNumber_shouldReturnCorrectCountedNumberOfZerosOfFactorial_whenCorrectNumberPassed() {
        int number = 10;

        int expectedZeros = counter.countLastZerosOfFactorialOfNumber(number);

        assertThat(expectedZeros).isEqualTo(2);
    }

    @Test
    public void countLastZerosOfFactorialOfNumber_shouldReturnCorrectCountedNumberOfZerosOfFactorial_whenPassedNumberIsZero() {
        int number = 0;

        int expectedZeros = counter.countLastZerosOfFactorialOfNumber(number);

        assertThat(expectedZeros).isEqualTo(1);
    }

    @Test
    public void countLastZerosOfFactorialOfNumber_shouldThrowNumberLessThanZeroExceptionWithCorrectMessage_whenPassedNumberIsZero() {
        int number = -1;

        NumberLessThanZeroException exception =
                assertThrows(NumberLessThanZeroException.class, () -> counter.countLastZerosOfFactorialOfNumber(number));

        assertThat(exception).hasMessage("Input number %d is less than zero. Please input correct value.", number);
    }
}