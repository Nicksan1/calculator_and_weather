package ru.teamidea.calculator.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.teamidea.calculator.service.ZeroCalculatorService;

import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("/calculator")
public class ZeroCalculatorRepository {

    private final ZeroCalculatorService zeroCalculatorService;

    @Autowired
    public ZeroCalculatorRepository(ZeroCalculatorService zeroCalculatorService) {
        this.zeroCalculatorService = zeroCalculatorService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer countZeros(@RequestParam @NotNull Integer number) {
        return zeroCalculatorService.countLastZerosOfFactorialOfNumber(number);
    }
}
