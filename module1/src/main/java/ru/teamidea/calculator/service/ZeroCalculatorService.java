package ru.teamidea.calculator.service;

import org.springframework.stereotype.Service;
import ru.teamidea.calculator.exception.NumberLessThanZeroException;

@Service
public class ZeroCalculatorService {

    public int countLastZerosOfFactorialOfNumber(int n) {
        if (n == 0) {
            return 1;
        } else if (n > 0) {
            return n / 5 + (n - (n % 25)) / 25;
        } else {
            String message = "Input number %d is less than zero. Please input correct value.";
            throw new NumberLessThanZeroException(String.format(message, n));
        }
    }
}
