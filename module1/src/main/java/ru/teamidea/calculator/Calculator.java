package ru.teamidea.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "ru.teamidea.calculator")
@EnableJpaRepositories("ru.teamidea.calculator.repository")
public class Calculator {

    public static void main(String[] args) {
        SpringApplication.run(Calculator.class, args);
    }
}
