package ru.teamidea.calculator.exception;

public class NumberLessThanZeroException extends RuntimeException {

    public NumberLessThanZeroException(String message) {
        super(message);
    }
}
