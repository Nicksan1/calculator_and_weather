package ru.teamidea.weather.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.teamidea.weather.exeption.WeatherDataNotFoundException;
import ru.teamidea.weather.model.OpenWeatherMapObject;
import ru.teamidea.weather.model.Weather;
import ru.teamidea.weather.model.WeatherParameters;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OpenWeatherMapService {

    private static final String TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(TIME_PATTERN);
    private static final int EVENING_TIME_BEGIN = 18;
    private static final int EVENING_TIME_END = 21;
    private static final double ABSOLUTE_ZERO_VALUE = -273.15;
    private static final int LIMIT_REQUEST_MINUTES = 10;

    @Value("${url}")
    private String url;

    @Value("${appid}")
    private String appid;

    private String id;
    private OpenWeatherMapObject openWeatherMapObject;
    private LocalDateTime requestTime;

    public int findMinPressure(String id) {
        return findWeather(id).getWeathers().stream()
                .map(Weather::getWeatherParameters)
                .map(WeatherParameters::getPressure)
                .mapToInt(Integer::intValue)
                .min()
                .orElseThrow(() -> new WeatherDataNotFoundException("Server has sent empty weather data"));
    }

    public double findAverageEveningFeelsLikeTemperature(String id) {
        List<Double> eveningFeelsLikeTemperatures = findWeather(id).getWeathers().stream()
                .filter(this::isEvening)
                .map(Weather::getWeatherParameters)
                .map(WeatherParameters::getFeelsLike)
                .collect(Collectors.toList());

        return convertToCelsius(eveningFeelsLikeTemperatures.stream()
                .mapToDouble(t -> t)
                .sum() / eveningFeelsLikeTemperatures.size());
    }

    private @NotNull OpenWeatherMapObject findWeather(String id) {
        if (requestTime == null || requestTime.isBefore(LocalDateTime.now().minusMinutes(LIMIT_REQUEST_MINUTES)) ||
                !id.equals(this.id)) {
            RestTemplate restTemplate = new RestTemplate();
            openWeatherMapObject = restTemplate.getForEntity(url, OpenWeatherMapObject.class, id, appid).getBody();
            requestTime = LocalDateTime.now();
            this.id = id;
        }
        return openWeatherMapObject;
    }

    private boolean isEvening(Weather weather) {
        LocalDateTime time = LocalDateTime.parse(weather.getTime(), FORMATTER);
        return time.getHour() < EVENING_TIME_BEGIN || time.getHour() > EVENING_TIME_END;
    }

    private double convertToCelsius(double kelvin) {
        return kelvin + ABSOLUTE_ZERO_VALUE;
    }
}
