package ru.teamidea.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "ru.teamidea.weather")
@EnableJpaRepositories("ru.teamidea.weather.repository")
public class OpenWeatherMapApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenWeatherMapApplication.class, args);
    }
}
