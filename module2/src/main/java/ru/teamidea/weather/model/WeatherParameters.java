package ru.teamidea.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherParameters {

    @JsonProperty("feels_like")
    private Double feelsLike;

    @JsonProperty("pressure")
    private Integer pressure;

    public Double getFeelsLike() {
        return feelsLike;
    }

    public Integer getPressure() {
        return pressure;
    }
}
