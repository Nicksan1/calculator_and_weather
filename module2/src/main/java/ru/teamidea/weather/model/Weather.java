package ru.teamidea.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    @JsonProperty("main")
    private WeatherParameters weatherParameters;

    @JsonProperty("dt_txt")
    private String time;

    public WeatherParameters getWeatherParameters() {
        return weatherParameters;
    }

    public String getTime() {
        return time;
    }
}
