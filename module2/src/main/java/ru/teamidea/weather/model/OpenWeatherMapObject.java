package ru.teamidea.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import java.util.List;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenWeatherMapObject {

    @JsonProperty("list")
    private List<Weather> weathers;

    public List<Weather> getWeathers() {
        return weathers;
    }
}
