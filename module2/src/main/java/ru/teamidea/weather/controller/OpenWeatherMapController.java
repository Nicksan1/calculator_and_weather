package ru.teamidea.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.teamidea.weather.service.OpenWeatherMapService;

import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("/weather")
public class OpenWeatherMapController {

    private final OpenWeatherMapService openWeatherMapService;

    @Autowired
    public OpenWeatherMapController(OpenWeatherMapService openWeatherMapService) {
        this.openWeatherMapService = openWeatherMapService;
    }

    @GetMapping(value = "/pressure", produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer findMinPressure(@RequestParam @NotNull String id) {
        return openWeatherMapService.findMinPressure(id);
    }

    @GetMapping(value = "/temperature", produces = MediaType.APPLICATION_JSON_VALUE)
    public Double findAverageEveningFeelsLikeTemperature(@RequestParam @NotNull String id) {
        return openWeatherMapService.findAverageEveningFeelsLikeTemperature(id);
    }
}
