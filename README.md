Module 1:
    Task #1 "Calculator"

    Calculates the last zeros of factorial of input number.

    How to use: Send the request http://localhost:8080/calculator?number={your number}

    Example: http://localhost:8080/calculator?number=100

Module 2:
    Task #2 "Weather"
    
    1. Getting minimal pressure for five days.
    2. Getting average evening feels like temperature.

    How to use: Send the request 
    1. http://localhost:8080/weather/pressure?id={your city id}
    2. http://localhost:8080/weather/temperature?id={your city id}

    Example: 
    1. http://localhost:8080/weather/pressure?id=524901
    2. http://localhost:8080/weather/temperature?id=524901

    P.S. To use your own appiid open application.properties and replace it.
    P.S.S. Download city.list.json.gz for getting other city ids from http://bulk.openweathermap.org/sample/
